# Modellus #

This is a repository containing Modellus public downloadable files.  
Check the [downloads tab](https://bitbucket.org/dukke/modellus-x-public-files/downloads/) for installers for the various versions of Modellus.